package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.util.Scanner;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.setName(name);
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() throws Exception{
    
       propertyRepository.addProperty(new Property(1,"address 1", 1, 100, 100.0));
       propertyRepository.addProperty(new Property(2,"address 2", 2, 200, 200.0));
       propertyRepository.addProperty(new Property(3,"address 3", 3, 300, 300.0));
       propertyRepository.addProperty(new Property(4,"address 4", 4, 400, 400.0));
       propertyRepository.addProperty(new Property(5,"address 5", 5, 500, 500.0));
       System.out.println("5 properties added successfully");
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception {
      for(Property p : propertyRepository.getAllProperties()) {
    	  System.out.println(p.toString());
    	  }
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
        @SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
        System.out.println("Enter the ID of he property you want to search:");
        String input = sc.nextLine();
        while (isNumeric(input) == false) {
        	System.out.println("Please enter number to search property:");
        	input = sc.nextLine();
        	}
       try {
       Property property = propertyRepository.searchPropertyById(Integer.parseInt(input)); 
	        if(property != null)
	        {
	        	System.out.println(property.toString());
	        
	        }
	        else System.out.println("Sorry, there is no property with number" + input);
        }catch (Exception ex)
       {
        	System.out.println("Sorry, there is no property with number" + input);
       }
        		
    }
    
    public static boolean isNumeric(String str) {
    	  for (int i = str.length(); --i >= 0;) {   
    	   if (!Character.isDigit(str.charAt(i))) {
    	    return false;
    	   }
    	  }
    	   return true;
    	}
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
