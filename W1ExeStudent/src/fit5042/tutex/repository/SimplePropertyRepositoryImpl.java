/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	ArrayList<Property> propertylist;
	
    public SimplePropertyRepositoryImpl() {
  propertylist = new ArrayList<Property>();
        
    }
    
    public void addProperty(Property property) throws Exception{
    	int propertyid = property.getId();
    	for( int i = 0; i < propertylist.size(); i++) {
    		if (propertylist.get(i).getId() == propertyid)
    		{propertylist.set(i, property);
    		return;
    		}
    	}
    	propertylist.add(property);
    }
    
    public Property searchPropertyById(int id) throws Exception{
    	for(Property p: propertylist) {
    		if(p.getId() == id) {
    			return p;
    		}
    	}
    	return null;
    	
    }

    public List<Property> getAllProperties() throws Exception{
    	return propertylist;
    }
}
