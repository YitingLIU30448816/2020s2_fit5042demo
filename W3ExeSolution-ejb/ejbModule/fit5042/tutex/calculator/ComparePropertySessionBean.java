package fit5042.tutex.calculator;


import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;
@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	
	private Set<Property> list;

	public ComparePropertySessionBean() {
		list = new HashSet<>();
	}

	@Override
	public void addProperty(Property property) {
		
		list.add(property);
	}

	@Override
	public int bestPerRoom() {
		int bestID = 0;
		int numberOfRooms;
		double price;
		double bestPerRoom = 10000000000.00;
		for(Property p:list)
		{
			numberOfRooms = p.getNumberOfBedrooms();
			price = p.getPrice();
			
			if (price / numberOfRooms < bestPerRoom)
			{
				bestPerRoom = price / numberOfRooms;
				bestID = p.getPropertyId();
			}
			
		}
		return bestID;
	}

	@Override
	public void removeProperty(Property property) {
		for (Property p : list) {
			if(p.getPropertyId() == property.getPropertyId()) {
				list.remove(p);
				break;
			}
			
		}
		
	}

//	@Override

    public CompareProperty create() throws CreateException, RemoteException {
        return null;
    }

	
	
}
